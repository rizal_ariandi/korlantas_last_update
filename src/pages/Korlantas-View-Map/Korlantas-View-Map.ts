import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ItemSliding } from 'ionic-angular';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    Marker,
    GoogleMapsAnimation,
    MarkerCluster,
    MyLocation
  } from '@ionic-native/google-maps';
@IonicPage()
@Component({
    selector: 'Korlantas-View-Map',
    templateUrl: 'Korlantas-View-Map.html'
})
export class KorlantasViewMap {
    data: any = []
    map: GoogleMap;
    constructor(public navCtrl: NavController, navParams: NavParams) {
    }
    ionViewWillEnter() {
        this.loadMap();
      }
    loadMap() {
        // Create a map after the view is loaded.
        // (platform is already ready in app.component.ts)
        this.map = GoogleMaps.create('map_canvas', {
          camera: {
            target: {
              lat: -6.176633,
              lng:  106.826966
            },
            zoom: 13            
          },
          styles: [
            {
              featureType: 'poi',
              elementType: 'labels',
              stylers: [
                {visibility: 'off'}
              ]
            }
          ],
          zoom:true
        });
        this.map.setTrafficEnabled(true);
        this.addCluster(this.dummyData());
      }

      addCluster(data) {
        let markerCluster: MarkerCluster = this.map.addMarkerClusterSync({
          markers: data,
          icons: [
            {
              min: 3,
              max: 9,
              url: "./assets/icon/small.png",
              label: {
                color: "white"
              }
            },
            {
              min: 10,
              url: "./assets/icon/large.png",
              label: {
                color: "white"
              }
            }
          ]
        });
    
        markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
          let marker: Marker = params[1];
          marker.setTitle(marker.get("name"));
          marker.setSnippet(marker.get("address"));
          marker.showInfoWindow();
        });
    
      }

      dummyData() {
        return [
          {
            "position": {
              "lat": -6.180290,
              "lng":  106.817764
            },
            "name": "CCTV1",
            "address": "Jakarta Pusat",
            "icon": "assets/icon/icon-cctv.png"
          },
          {
            "position": {
              "lat": -6.180664, 
              "lng": 106.823025
            },
            "name": "CCTV 2",
            "address": "Jakarta Pusat",
            "icon": "assets/icon/icon-cctv.png"
          },
          {
            "position": {
              "lat": -6.171183,
              "lng": 106.829421
            },
            "name": "CCTV 3",
            "address": "Jakarta Pusat",
            "icon": "assets/icon/icon-cctv.png"
          },
          {
            "position": {
              "lat": -6.169164,
              "lng":  106.821319
            },
            "name": "Polisi",
            "address": "Jakarta Pusat",
            "icon": "assets/icon/policeofficer_man_person_polici.png"
          },
          {
            "position": {
              "lat": -6.182605,
              "lng":   106.833308
                       },
            "name": "Polisi",
            "address": "Jakarta Pusat",
            "icon": "assets/icon/policeofficer_man_person_polici.png"
          },      
        ];
      }
}

