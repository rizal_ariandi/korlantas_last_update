import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KorlantasViewMap } from './Korlantas-View-Map'


@NgModule({
  declarations: [
    KorlantasViewMap,
  ],
  imports: [
    IonicPageModule.forChild(KorlantasViewMap),    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class KorlantasViewMapModule { }
