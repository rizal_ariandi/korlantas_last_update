import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KorlantasListCCTV } from './Korlantas-list-CCTV';


@NgModule({
  declarations: [
    KorlantasListCCTV,
  ],
  imports: [
    IonicPageModule.forChild(KorlantasListCCTV),    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class KorlantasListCCTVModule { }
