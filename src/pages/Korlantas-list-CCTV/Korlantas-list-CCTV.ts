import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ItemSliding } from 'ionic-angular';
import { VideoPlayer } from '@ionic-native/video-player';
@IonicPage()
@Component({
    selector: 'Korlantas-list-CCTV',
    templateUrl: 'Korlantas-list-CCTV.html'
})
export class KorlantasListCCTV {
    data: any = []
    constructor(public navCtrl: NavController, navParams: NavParams, private videoPlayer: VideoPlayer) {
        this.data =
            [
                {
                    "id": 1,
                    "title": "CCTV 1",
                    "subtitle": "Jl. Gatot Subroto, Daerah Khusus Ibukota Jakarta",
                    "image": "assets/images/background/cctv1.jpg",
                    "delate": "Choose",
                    'src': "KorlantasViewCCTV"
                },
                {
                    "id": 2,
                    "title": "CCTV 2",
                    "subtitle": "Jl. Jend. Sudirman, Daerah Khusus Ibukota Jakarta",
                    "image": "assets/images/background/cctv2.jpg",
                    "delate": "Choose",
                    'src': "KorlantasViewCCTV"
                },
                {
                    "id": 3,
                    "title": "CCTV 3",
                    "subtitle": "Jl. Panjaitan, Daerah Khusus Ibukota Jakarta",
                    "image": "assets/images/background/cctv3.jpg",
                    "delate": "Choose",
                    'src': "KorlantasViewCCTV"
                }
            ];
    };

    viewCCTV() { 
        this.videoPlayer.play('file:///android_asset/www/assets/video/cctv.mp4').then(() => {
            console.log('video completed');
        }).catch(err => {
            console.log(err);
        });
        // console.log('video completed');
    }

    undo = (slidingItem: ItemSliding) => {
        slidingItem.close();
    }

    Choose = (item: any): void => {
        let index = this.data.indexOf(item);
        if (index > -1) {
            this.data.splice(index, 1);
        }
    }
}
